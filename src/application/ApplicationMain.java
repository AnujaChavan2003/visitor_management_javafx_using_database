package application;

import java.io.IOException;

import db_operations.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import show_option.ShowOption;
import stage_master.StageMaster;

public class ApplicationMain extends Application {
	
	public static void main(String[] args) throws IOException {
		DbUtil.createDbConnection();
		launch(args);
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		System.out.println("Application started");
		new ShowOption().show();
		System.out.println("Application stop");

	}

}
