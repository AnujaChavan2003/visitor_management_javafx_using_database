package db_operations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {

	static Connection con;
	static Statement stmt;
	public static void createDbConnection() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Visitor_Management","root","Anuja@2003");
			stmt = con.createStatement();
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}
	public static void executeQuery(String query) {
		try {
			stmt.execute(query);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}

		
	}
}
